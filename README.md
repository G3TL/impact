# Impact

Impact is a LibreWolf-based browser. Meant for privacy nerds.

## Why?

Impact doesn't sell your info. You also build it yourself! Impact is meant for privacy nerds, as I said.
